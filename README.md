![IMG_20160419_100003051.jpg](https://bitbucket.org/repo/ykRdKE/images/1483085039-IMG_20160419_100003051.jpg)

## Working on this Project ##
* Put all assets into the Assets folder in the "Card Matcher" directory.
* Put your SFML directory "SFML-2.3.2-windows-vc14-32-bit" right next to the "Card Matcher" directory. The "readme.txt" should be at the root directory of it.
* The Debug and Release folders (and all other temporary folders that don't need to be on version control) are ignored in git.
* The assets and necessary dlls are copied into the respective output folders when building (so make sure you have enough hard drive space).
* Try not to add or change too many files in the Assets folder. Assets can't merge and every change will increase the size of the history more than it should.

## Turning it in ##
* Go to Downloads > Download Repository in the sidebar on the left. This doesn't download the 100 MB Bieber .wav file that was there or any other git history.
* Put the SFML folder in the same place again and maybe build the game again (under Release).
* Zip it up and stuff according to the same instructions on the PA9 page.
* Remember that x64 compiling does not work (for obvious reasons).

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact