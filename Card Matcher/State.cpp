#include "State.h"

State::~State()
{
	for (unsigned int i = 0; i < gameObjects.size(); i++)
		delete gameObjects[i];

	gameObjects.clear();
}

void State::HandleEvents(Game *game, sf::Event &events)
{
	for (unsigned int i = 0; i < gameObjects.size(); i++)
		gameObjects[i]->HandleEvents(game, events);
}

void State::Update(Game *game)
{
	for (unsigned int i = 0; i < gameObjects.size(); i++)
		gameObjects[i]->Update(game);
}

void State::Draw(Game *game) const
{
	for (unsigned int i = 0; i < gameObjects.size(); i++)
		gameObjects[i]->Draw(game);
}

void State::Add(GameObject *gameObject)
{
	gameObjects.push_back(gameObject);
	gameObject->state = this;
}
void State::DeleteObj(GameObject *gameObject)
{
	for (unsigned int i = 0; i < gameObjects.size(); i++)
	{
		if (gameObjects[i] == gameObject)
		{
			delete gameObjects[i];
			gameObjects.erase(gameObjects.begin() + i);
		}
	}
}