#include "Menu.h"
#include "PlayState.h"

Menu::Menu(sf::Vector2f &position, sf::Vector2f &size)
{
	setPosition(position);
	this->size = size;
	fontCharacterSize = 50;
	selectedColor = sf::Color::White;
	normalColor = sf::Color::Red;
	selectedEntryIndex = 0;

	if (!font.loadFromFile("Assets/PixelMplus12-Regular.ttf"))
	{
		// Error handling
	}

	std::string entryNames[] =
	{
		"Level One",
		"Level Two",
		"Level Three",
		"Exit"
	};

	for (int i = 0; i < 4; i++)
		entries.push_back(MakeEntry(entryNames[i]));
	
	RealignEntries();
	UpdateEntryColors();
}

Menu::~Menu()
{

}

// TODO: Use an input manager so we don't directly use keyboard codes
void Menu::HandleEvents(Game *game, sf::Event &events)
{
	// Handle player input
	if (events.type == events.KeyPressed)
	{
		switch (events.key.code)
		{
		case sf::Keyboard::Up:
			MoveUp();
			UpdateEntryColors();
			break;
		case sf::Keyboard::Down:
			MoveDown();
			UpdateEntryColors();
			break;
		case sf::Keyboard::Space:
		case sf::Keyboard::Return:
		case sf::Keyboard::Right:
			if (selectedEntryIndex < 3)
				game->SwitchState(new PlayState(game,this->selectedEntryIndex));
			else
				game->closing = true;
			break;
		case sf::Keyboard::Escape:
			game->closing = true;
			break;
		default:
			break;
		}
	}
	sf::Vector2f mouseVector((float)events.mouseButton.x, (float)events.mouseButton.y);
	for (int i = 0; i < 3; i++)
	{
		if ( (&entries[i])->getGlobalBounds().contains(mouseVector) )
		{
			game->SwitchState(new PlayState(game,i));
		}
	}
	if ((&entries[3])->getGlobalBounds().contains(mouseVector))
	{
		game->closing = true;
	}
}

void Menu::Update(Game *game)
{
	// Do game logic
}

void Menu::Draw(Game *game) const
{
	// Draw things
	for (unsigned int i = 0; i < entries.size(); i++)
		game->renderWindow.draw(entries[i]);
}

void Menu::AddEntry(const std::string &entryName)
{
	entries.push_back(MakeEntry(entryName));
	RealignEntries();
}

sf::Text Menu::MakeEntry(const std::string &entryName)
{
	sf::Text newEntry;
	newEntry.setFont(font);
	newEntry.setColor(normalColor);
	newEntry.setString(entryName);
	newEntry.setCharacterSize(fontCharacterSize);

	float width = newEntry.getGlobalBounds().width;
	float x = this->getPosition().x + (size.x - width) / 2;
	newEntry.setPosition(x, 0);

	return newEntry;
}

void Menu::RealignEntries()
{
	for (unsigned int i = 0; i < entries.size(); i++)
	{
		float height = (float)entries[i].getCharacterSize();
		float heightOffset = -5;
		float y = this->getPosition().y;

		if (entries.size() > 1)
		{
			y += this->size.y * ((float)i / (entries.size() - 1));
			y -= (height - heightOffset) * ((float)i / (entries.size() - 1));
		}

		entries[i].setPosition(entries[i].getGlobalBounds().left, y);
	}
}

void Menu::MoveUp()
{
	if (selectedEntryIndex > 0)
		selectedEntryIndex--;
	else
		selectedEntryIndex = entries.size() - 1;
}

void Menu::MoveDown()
{
	if (selectedEntryIndex < entries.size() - 1)
		selectedEntryIndex++;
	else
		selectedEntryIndex = 0;
}

void Menu::UpdateEntryColors()
{
	for (unsigned int i = 0; i < entries.size(); i++)
		entries[i].setColor(normalColor);

	entries[selectedEntryIndex].setColor(selectedColor);
}
