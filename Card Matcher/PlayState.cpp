#include "PlayState.h"
#include "Card.h"
#include "Background.h"
#include "MenuState.h"

PlayState::PlayState(Game *game, int level)
{
	std::string bgName = "Themed_Cards/Disney_backdrop.jpg";
	pickedChoices = { -1,-1, -1 };
	Background *bg = new Background(sf::Vector2f(0, 0), bgName);
	bg->Resize(sf::Vector2f(game->renderWindow.getSize()));
	Add(bg);

	int cardColumns, cardRows;

	//add card textures based on the level's theme
	sf::Texture *frontSpriteTexture;
	switch (level)
	{
	case 0: // 8 cards
		for (int cardIndex = 0; cardIndex < 4; cardIndex++)
		{
			std::string imgName = "Assets/Themed_Cards/ariel" + std::to_string(cardIndex + 1) + ".jpg";
			frontSpriteTexture = new sf::Texture();
			frontSpriteTexture->loadFromFile(imgName);
			cardTextures.push_back(frontSpriteTexture);
			cardTextures.push_back(frontSpriteTexture);
		}

		cardColumns = 4;
		cardRows = 2;
		break;
	case 1: // 20 cards
		for (int cardIndex = 0; cardIndex < 10; cardIndex++)
		{
			std::string imgName = "Assets/Themed_Cards/LK" + std::to_string(cardIndex + 1) + ".jpg";
			frontSpriteTexture = new sf::Texture();
			frontSpriteTexture->loadFromFile(imgName);
			cardTextures.push_back(frontSpriteTexture);
			cardTextures.push_back(frontSpriteTexture);
		}

		cardColumns = 5;
		cardRows = 4;
		break;
	case 2: // 32 cards
		for (int cardIndex = 0; cardIndex < 16; cardIndex++)
		{
			std::string imgName = "Assets/Themed_Cards/mulan" + std::to_string(cardIndex) + ".jpg";
			frontSpriteTexture = new sf::Texture();
			frontSpriteTexture->loadFromFile(imgName);
			cardTextures.push_back(frontSpriteTexture);
			cardTextures.push_back(frontSpriteTexture);
		}

		cardColumns = 8;
		cardRows = 4;
		break;
	}

	std::random_shuffle(cardTextures.begin(), cardTextures.end());


	backSpriteTexture = new sf::Texture();
	//backSpriteTexture->loadFromFile("Assets/back_no_whitespace.png");
	backSpriteTexture->loadFromFile("Assets/Themed_cards/Disney2.jpg");

	// level (0-2)
	//std::cout << "LEVEL " << level + 1 << std::endl;
	int gameWidth = game->renderWindow.getSize().x;
	int gameHeight = game->renderWindow.getSize().y;
	float paddingX = 8;
	float paddingY = 6;

	Card tempCard(sf::Vector2f(0.f, 0.f), cardTextures.back(), backSpriteTexture);
	float cardWidth = tempCard.GetSize().x;
	float cardHeight = tempCard.GetSize().y;

	sf::Vector2f startingPosition
	(
		(gameWidth - (paddingX + cardWidth) * cardColumns) / 2,
		(gameHeight - (paddingY + cardHeight) * cardRows) / 2
	);

	int cardIndex = 0;

	for (int y = 0; y < cardRows; y++)
	{
		for (int x = 0; x < cardColumns; x++)
		{
			sf::Vector2f padding
			(
				(paddingX + cardWidth) * x,
				(paddingY + cardHeight) * y
			);

			Card *newCard = new Card(startingPosition + padding, cardTextures[cardIndex], backSpriteTexture);
			Add(newCard);
			cards.push_back(newCard);
			cardIndex++;
		}
	}
	//it takes a bit to start streaming the music so let's do that
	//once the cards have been loaded
	PlayMusic(level);
}

PlayState::~PlayState()
{
	// Cards themselves are deleted in the base State
	cards.clear();
	delete backSpriteTexture; // now we only delete the back texture once
	delete music;
	//delete every other card in the sorted vector
	std::sort(cardTextures.begin(), cardTextures.end());
	for (unsigned int i = 0; i < cardTextures.size(); i+=2)
	{
		delete cardTextures[i];
	}
}

void PlayState::PlayMusic(int level)
{
	music = new sf::Music();
	switch (level)
	{
	case 0:
		music->openFromFile("Assets/little-mermaid.ogg");
		break;
	case 1:
		music->openFromFile("Assets/lion-king.ogg");
		break;
	case 2:
		music->openFromFile("Assets/mulan.ogg");
		break;
	}
	music->play();
	music->setLoop(true);
}

void PlayState::HandleEvents(Game *game, sf::Event &events)
{
	if (events.type == events.KeyPressed &&
		events.key.code == sf::Keyboard::Escape)
		game->SwitchState(new MenuState(game));

	for (unsigned int i = 0; i < gameObjects.size(); i++)
		gameObjects[i]->HandleEvents(game, events);
}

void PlayState::Update(Game *game)
{
	for (unsigned int i = 0; i < gameObjects.size(); i++)
		gameObjects[i]->Update(game);


	for (unsigned int i = 0; i < cards.size(); i++)
	{
		if (cards[i]->cardSide == front && !(cards[i]->flipping))
		{
			if (pickedChoices.x != i && pickedChoices.y != i && pickedChoices.z != i)
			{
				if (pickedChoices.x < 0)
				{
					pickedChoices.x = i;
				}
				else if (pickedChoices.y < 0)
				{
					pickedChoices.y = i;
				}
				else if (pickedChoices.z < 0)
				{
					pickedChoices.z = i;
				}
			}
		}
	}
	if (pickedChoices.x >= 0 && pickedChoices.y >= 0)
	{
		//two cards have been flipped
		if (cards[pickedChoices.x]->frontSpriteTexture == cards[pickedChoices.y]->frontSpriteTexture)
		{
			//it's a match!
			//delete them
			//delete cards[pickedChoices.x];
			//delete cards[pickedChoices.y];
			DeleteObj(cards[pickedChoices.x]);
			DeleteObj(cards[pickedChoices.y]);
			if (pickedChoices.x < pickedChoices.y)
			{//delete y first (it won't screw up the index
				cards.erase(cards.begin() + pickedChoices.y);
				cards.erase(cards.begin() + pickedChoices.x);
			}
			else
			{
				cards.erase(cards.begin() + pickedChoices.x);
				cards.erase(cards.begin() + pickedChoices.y);

			}
			pickedChoices = { -1,-1,-1 };
		}
		else
		{
			//they don't match
			//flip them over
			if (pickedChoices.z >= 0)
			{
				cards[pickedChoices.x]->flipping = true;
				cards[pickedChoices.y]->flipping = true;
				pickedChoices = { pickedChoices.z,-1,-1 };
			}
		}
	}
	if (cards.size() == 0)
	{
		//escape
		game->SwitchState(new MenuState(game));
	}
}

// This doesn't do anything
void PlayState::SetChoice()
{
	std::vector<int> myvector;

	for (int i = 0; i < 4; i++)
	{
		myvector.push_back(i);
		myvector.push_back(i);
	}

	std::random_shuffle(myvector.begin(), myvector.end());

	for (int i = 0; i < 8; i++)
		choices[i] = myvector[i];
}