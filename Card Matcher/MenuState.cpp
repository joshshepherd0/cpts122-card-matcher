#include "MenuState.h"
#include "Menu.h"
#include "Background.h"

MenuState::MenuState(Game *game)
{
	std::string bgName = "Themed_Cards/Disney_backdrop.jpg";

	Background *bg = new Background(sf::Vector2f(0, 0), bgName);
	bg->Resize(sf::Vector2f(game->renderWindow.getSize()));

	Menu *menu = new Menu
	(
		sf::Vector2f(0, 20),
		sf::Vector2f(game->renderWindow.getSize()) - sf::Vector2f(0, 20 * 2)
	);

	Add(bg);
	Add(menu);
}