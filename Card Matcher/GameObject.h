#pragma once

#include <vector>
#include "Game.h"
#include "State.h"
#include "SFML/Graphics.hpp"

class Game;
class State;

// GameObject
// You should really just inherit from this if you want to make things
class GameObject : public sf::Transformable
{
public:
	State* state; // The State the GameObject was added to

	GameObject(sf::Vector2f &position = sf::Vector2f(0, 0));
	virtual ~GameObject();

	virtual void HandleEvents(Game *game, sf::Event &events);
	virtual void Update(Game *game);
	virtual void Draw(Game *game) const;
private:
};