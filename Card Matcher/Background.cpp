#include "Background.h"

Background::Background(sf::Vector2f &position, std::string bgName)
{
	texture = new sf::Texture;

	if (bgName != "")
		texture->loadFromFile("Assets/" + bgName);

	sprite = new sf::Sprite(*texture);
}

Background::~Background()
{
	delete texture;
	delete sprite;
}

void Background::HandleEvents(Game *game, sf::Event &events)
{
	// Handle player input
}

void Background::Update(Game *game)
{
	// Do game logic
}

void Background::Draw(Game *game) const
{
	game->renderWindow.draw(*sprite);
}

void Background::Resize(const sf::Vector2f &size)
{
	sprite->setScale
	(
		size.x / sprite->getLocalBounds().width,
		size.y / sprite->getLocalBounds().height
	);
}