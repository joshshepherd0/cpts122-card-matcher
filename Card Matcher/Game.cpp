#include "Game.h"
#include "MenuState.h"

Game::Game()
{
	closing = false;
	frameRate = 60;
	std::srand((unsigned int)std::time(0));

	sf::Uint32 style =
		//sf::Style::None; // Presentation Mode
		sf::Style::Titlebar | sf::Style::Close; // Normal
		//sf::Style::Fullscreen; // Fullscreen

	renderWindow.create(sf::VideoMode(800, 600), "Card Matcher", style);
	renderWindow.setKeyRepeatEnabled(false);
	renderWindow.setFramerateLimit(frameRate);

	AddState(new MenuState(this));
}

Game::~Game()
{
	for (unsigned int i = 0; i < states.size(); i++)
		delete states[i];

	states.clear();
}

void Game::Run()
{
	sf::Clock clock;
	int timePassed = 0;
	int numFrameChecks = 0;
	int maxFrameChecks = 10;
	int fps = 0;

	while (renderWindow.isOpen())
	{
		Loop(clock);

		timePassed += (int)clock.restart().asMicroseconds();
		numFrameChecks++;

		if (numFrameChecks >= maxFrameChecks)
		{
			fps = (int)(1000000.f / ((double)timePassed / numFrameChecks));
			std::cout << fps << " fps" << std::endl;
			timePassed = 0;
			numFrameChecks = 0;
		}
	}
}

void Game::AddState(State *state)
{
	state->game = this;
	states.push_back(state);
}

void Game::SwitchState(State *state)
{
	changedState = true; // Will delete after being done processing just like with closing
	oldState = states.back(); // Keep track of state for when it's time to delete
	states.pop_back(); // All this does is delete the pointer, you need to delete the state itself too
	AddState(state);
}

void Game::Loop(sf::Clock deltaTime)
{
	sf::Event event;

	while (renderWindow.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			closing = true;
		}
		if (event.type == sf::Event::Resized)
		{
			sf::FloatRect visibleArea(0, 0, (float)event.size.width, (float)event.size.height);
			renderWindow.setView(sf::View(visibleArea));
		}

		states.back()->HandleEvents(this, event);
	}

	states.back()->Update(this);
	renderWindow.clear();
	states.back()->Draw(this);
	renderWindow.display();

	if (changedState)
	{
		delete oldState;
		changedState = false;
	}

	if (closing)
	{
		for (unsigned int i = 0; i < states.size(); i++)
			delete states[i];

		states.clear();
		renderWindow.close();
	}
}