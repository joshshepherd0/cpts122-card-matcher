#include "Card.h"
#include <string>
#include <vector>

Card::Card(sf::Vector2f &position, sf::Texture * newFrontSpriteTexture, sf::Texture * backSpriteTexture)
{
	this->frontSpriteTexture = newFrontSpriteTexture;
	frontSprite = new sf::Sprite(*frontSpriteTexture);
	backSprite = new sf::Sprite(*backSpriteTexture);

	//we should resize these Themed Cards so they aren't stretched
	/*texture = new sf::Texture();
	texture->loadFromFile("assets/triangle.png");
	frontSpriteTexture = new sf::Texture();

	frontSprite = new sf::Sprite(*texture);
	backSprite = new sf::Sprite(*texture);*/

	//backSprite->setScale(sf::Vector2f(.75f, .75f)); Fullscren 1920x1080
	backSprite->setScale(sf::Vector2f(.4f, .4f));
	sf::FloatRect targetSize = backSprite->getGlobalBounds();

	//get the right scale so that front sprite is the same size as the back sprite
	frontSprite->setScale
	(
		targetSize.width / frontSprite->getLocalBounds().width,
		targetSize.height / frontSprite->getLocalBounds().height
	);

	// Keep original scale sizes for transformation
	originalFrontScale = frontSprite->getScale();
	originalBackScale = backSprite->getScale();

	// Center origin
	frontSprite->setOrigin(frontSprite->getLocalBounds().width / 2, frontSprite->getLocalBounds().height / 2);
	backSprite->setOrigin(backSprite->getLocalBounds().width / 2, backSprite->getLocalBounds().height / 2);

	// Readjust position based on origin so the position corresponds to the upper-left corner
	frontSprite->setPosition
	(
		position.x + frontSprite->getOrigin().x * originalFrontScale.x,
		position.y + frontSprite->getOrigin().y * originalFrontScale.y
	);

	backSprite->setPosition
	(
		position.x + backSprite->getOrigin().x * originalBackScale.x,
		position.y + backSprite->getOrigin().y * originalBackScale.y
	);

	frameCounter = 0;
	flipping = false;
	cardSide = back;
}

Card::~Card()
{
	delete frontSprite;
	delete backSprite;
	//delete frontSpriteTexture;
	//delete backSpriteTexture; //now every card isn't going to try to delete our texture
}

void Card::HandleEvents(Game *game, sf::Event &events)
{
	if (events.type == events.MouseButtonPressed)
	{
		//frontSprite->getGlobalBounds().contains(sf::Vector2f((float)events.mouseButton.x, (float)events.mouseButton.y)) ||
		if (backSprite->getGlobalBounds().contains(sf::Vector2f((float)events.mouseButton.x, (float)events.mouseButton.y)))
		{
			flipping = true;
		}
	}
}

void Card::Update(Game *game)
{
	if (flipping)
	{
		if (cardSide == front)
		{
			if (elapsedFlipTime * 2 < timeToFlip)
			{
				backSprite->setScale(sf::Vector2f(0, originalBackScale.y));
				frontSprite->setScale(sf::Vector2f((1 - 2 * ((float)elapsedFlipTime / timeToFlip)) * originalFrontScale.x, originalFrontScale.y));
			}
			else
			{
				frontSprite->setScale(sf::Vector2f(0, originalFrontScale.y));
				backSprite->setScale(sf::Vector2f((-1 + 2 * ((float)elapsedFlipTime / timeToFlip)) * originalBackScale.x, originalBackScale.y));
			}
		}
		else
		{
			if (elapsedFlipTime * 2 < timeToFlip)
			{
				frontSprite->setScale(sf::Vector2f(0, originalFrontScale.y));
				backSprite->setScale(sf::Vector2f((1 - 2 * ((float)elapsedFlipTime / timeToFlip)) * originalBackScale.x, originalBackScale.y));
			}
			else
			{
				backSprite->setScale(sf::Vector2f(0, originalBackScale.y));
				frontSprite->setScale(sf::Vector2f((-1 + 2 * ((float)elapsedFlipTime / timeToFlip)) * originalFrontScale.x, originalFrontScale.y));
			}
		}

		elapsedFlipTime = (float)frameCounter / 60;
		frameCounter++;

		if (elapsedFlipTime >= timeToFlip)
		{
			if (cardSide == front)
			{
				backSprite->setScale(originalBackScale);
				cardSide = back;
			}
			else
			{
				frontSprite->setScale(originalFrontScale);
				cardSide = front;
			}

			flipping = false;
			elapsedFlipTime = 0;
			frameCounter = 0;
		}
	}
}

void Card::Draw(Game *game) const
{
	game->renderWindow.draw(*frontSprite);
	game->renderWindow.draw(*backSprite);
}

sf::Vector2f Card::GetSize() const
{
	sf::Vector2f size = sf::Vector2f
	(
		frontSprite->getGlobalBounds().width,
		frontSprite->getGlobalBounds().height
	);

	return size;
}

bool Card::IsBeingFlipped() const
{
	return flipping;
}

void Card::SetFlipping(bool flipping)
{
	this->flipping = flipping;
}