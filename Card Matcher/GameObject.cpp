#include "GameObject.h"

GameObject::GameObject(sf::Vector2f &position)
{
	setPosition(position);
}

GameObject::~GameObject()
{

}

void GameObject::HandleEvents(Game *game, sf::Event &events)
{
	// Handle player input
}

void GameObject::Update(Game *game)
{
	// Do game logic
}

void GameObject::Draw(Game *game) const
{
	// Draw things
}