#pragma once

#include "State.h"
#include "SFML/Audio.hpp"

class Card;

class PlayState : public State
{
public:
	PlayState(Game *game, int level);
	virtual ~PlayState();
	void HandleEvents(Game *game, sf::Event &events);
	void Update(Game *game);
	void SetChoice();
private:
	int choices[8];
	sf::Vector3i pickedChoices;
	std::vector<Card *> cards;
	sf::Texture *backSpriteTexture;
	sf::Music *music;
	std::vector<sf::Texture *> cardTextures;

	void PlayMusic(int level);
};