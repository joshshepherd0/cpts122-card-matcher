#pragma once

#include "GameObject.h"

enum Side
{
	front,
	back
};

class Card : public GameObject
{
public:
	std::string mPicName;
	Side cardSide;
	bool flipping;

	Card(sf::Vector2f &position, sf::Texture * frontSpriteTexture, sf::Texture * backSpriteTexture);
	virtual ~Card();

	void HandleEvents(Game *game, sf::Event &events);
	void Update(Game *game);
	void Draw(Game *game) const;
	void draw(sf::RenderTarget &target, sf::RenderStates states) const {};

	sf::Vector2f GetSize() const;
	bool IsBeingFlipped() const;
	void SetFlipping(bool flipping = true);
	sf::Texture *frontSpriteTexture;
private:
	//sf::Texture *backSpriteTexture; //we only need to make one backSpriteTexture
	sf::Sprite *frontSprite;
	sf::Sprite *backSprite;

	int frameCounter;

	float timeToFlip = 0.2f;
	float elapsedFlipTime = 0.f;

	sf::Vector2f originalBackScale;
	sf::Vector2f originalFrontScale;
};