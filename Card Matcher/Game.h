#pragma once

#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include "State.h"

class State;

// Window and state manager
class Game
{
public:
	sf::RenderWindow renderWindow;
	bool closing;
	int frameRate;

	Game();
	~Game();

	void Run();
	void AddState(State *state);
	void SwitchState(State *state);
private:
	std::vector<State *> states;
	bool changedState = false;
	State *oldState;

	void Loop(sf::Clock deltaTime);
};