#pragma once

#include "Game.h"
#include "GameObject.h"
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"

class Game;
class GameObject;

class State
{
public:
	Game *game;
	std::vector<GameObject*> gameObjects;

	//State();
	virtual ~State(); // Destroy all those gameObjects

	// Do everything to every object
	virtual void HandleEvents(Game *game, sf::Event &events);
	virtual void Update(Game *game);
	virtual void Draw(Game *game) const;

	// Add GameObject to State so we can do things to it
	void Add(GameObject *gameObject);
	void DeleteObj(GameObject *gameObject);
private:
};