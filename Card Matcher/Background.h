#pragma once

#include "GameObject.h"

class Background : public GameObject
{
public:
	Background(sf::Vector2f &position = sf::Vector2f(0, 0), std::string bgName = "");
	virtual ~Background();

	void HandleEvents(Game *game, sf::Event &events);
	void Update(Game *game);
	void Draw(Game *game) const;
	void draw(sf::RenderTarget &target, sf::RenderStates states) const {};

	void Resize(const sf::Vector2f &size);
private:
	sf::Texture *texture;
	sf::Sprite *sprite;
};