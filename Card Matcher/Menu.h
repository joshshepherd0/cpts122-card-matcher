#pragma once

#include "GameObject.h"
#include <iostream>

class Menu : public GameObject
{
public:
	sf::Vector2f size;

	Menu(sf::Vector2f &position = sf::Vector2f(0, 0), sf::Vector2f &size = sf::Vector2f(0, 0));
	virtual ~Menu();

	virtual void HandleEvents(Game *game, sf::Event &events);
	virtual void Update(Game *game);
	virtual void Draw(Game *game) const;
private:
	
	sf::Font font;
	sf::Color selectedColor;
	sf::Color normalColor;
	unsigned int fontCharacterSize;
	std::vector<sf::Text> entries;
	unsigned int selectedEntryIndex;

	void AddEntry(const std::string &entryName);
	sf::Text MakeEntry(const std::string &entryName);
	void RealignEntries();
	void MoveUp();
	void MoveDown();
	void UpdateEntryColors();
};